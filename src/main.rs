use std::mem::take;

fn case1() {
    let x: i32 = 5;
    let y: i32 = x;

    let s1: String = String::from("hello");
    let s2: String = s1.clone();

    println!("{}, world!", s1);
}

fn case2() {
    let mut s: String = String::from("hello");
    s = takes_ownership(s);
    println!("1 {}", s);
}

fn takes_ownership(some_string: String) -> String {
    println!("2 {}", some_string);
    return some_string;
}


fn main() {
    //case1();
    case2();
}
